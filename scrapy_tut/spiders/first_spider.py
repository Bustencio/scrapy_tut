import scrapy
from scrapy_tut.items import ScrapyTutItem

class FirstSpider(scrapy.Spider):
    name = "first"
    allowed_domains = ["dmoz.org"]

    start_urls = [ 
      "http://dmoz-odp.org/Computers/Programming/Languages/Python/",  
    ] 

    def parse(self, response):
      hrefs = response.xpath('//div[contains(@class,"cat-item")]//a/@href')
      for href in hrefs:
        url = response.urljoin(href.extract())
        print (url)
        yield scrapy.Request(url, callback = self.parse_info, dont_filter=True)
    

    def parse_info(self, response):
      sels = response.xpath('//div[contains(@class,"site-item")]//div[contains(@class,"title-and-desc")]')
      for sel in sels:
        item = ScrapyTutItem()
        item['link'] = sel.xpath('a/@href').extract()
        item['title'] = sel.xpath('a//div[contains(@class,"site-title")]/text()').extract()
        item['desc'] = sel.xpath('div[contains(@class,"site-desc")]/text()').extract()
        yield item